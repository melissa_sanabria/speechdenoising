import glob
import xml.etree.ElementTree as ET

xml_path_files = "/data_B/sanabria/premier_league/opta/"
teams_stats = {}
player_stats = {}
player_team = {}
for xml_file in sorted(glob.glob(xml_path_files + "*")):
    video_id = xml_file.split("/")[-1].split(".")[0]
    tree = ET.parse(xml_file)
    root = tree.getroot()
    away_team = root[0].attrib["away_team_name"]
    home_team = root[0].attrib["home_team_name"]
    away_team_id = root[0].attrib["away_team_id"]
    home_team_id = root[0].attrib["home_team_id"]

    if away_team not in teams_stats.keys():
        teams_stats[away_team] = 0

    if home_team not in teams_stats.keys():
        teams_stats[home_team] = 0

    teams_stats[away_team] +=1
    teams_stats[home_team] +=1

    print video_id + "," + away_team + "," + home_team

    for i in range(0, len(root[0])):
        if "player_id" in root[0][i].attrib.keys():
            player_id = int(root[0][i].attrib["player_id"])

            if player_id not in player_stats.keys():
                player_stats[player_id] = 0

                if int(away_team_id) == int(root[0][i].attrib["team_id"]):
                    player_team[player_id] = away_team
                if int(home_team_id) == int(root[0][i].attrib["team_id"]):
                    player_team[player_id] = home_team

            player_stats[player_id] += 1

import operator
teams_stats = sorted(teams_stats.items(), key=operator.itemgetter(1), reverse=True)
player_stats = sorted(player_stats.items(), key=operator.itemgetter(1), reverse=True)

for team, val in teams_stats:
    print team + "," + str(val)

for player, val in player_stats:
    print str(player) + "," + str(val) + "," + player_team[player]