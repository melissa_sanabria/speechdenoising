import json
import models
import pickle
import numpy as np
import os
import glob
import util
import denoise

from keras.optimizers import adam

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "2"

config = json.load(open("config.json"))

mode = "training"
# mode = "inference"


def batch_generator(X, padded_target_field_indices):
    while True:
        sample_indices = np.random.randint(0, config["training"]["num_train_samples"],
                                           config["training"]["batch_size"])

        yield X[sample_indices, 0, :], X[sample_indices, 1, :][:, padded_target_field_indices]

if mode == "training":
    model = models.DenoisingWavenet(config, print_model_summary=False)

    ## These are the arrays coming from the dataset creation. They should be of size (#samples, 2, 7739) where
    ## train_data[:, 0, :] is the original audio and train_data[:, 1, :] is the only commentators
    
    with open('/data/diop/noise_removal/train_data.pkl','rb') as f1:
    	train_data = pickle.load(f1)

    with open('/data/diop/noise_removal/test_data.pkl','rb') as f2:
	    test_data = pickle.load(f2)

    ## These are random numbers just to see if the code is working
    # train_data = np.random.rand(100, 2, 7739)
    # test_data = np.random.rand(20, 2, 7739)
    print(np.shape(train_data))
    print(np.shape(test_data))

    test_data = test_data[:config["training"]["num_test_samples"]]

    # train_noise_samples = train_data[:, 0, :] - train_data[:, 1, :]
    # test_noise_samples = test_data[:, 0, :] - test_data[:, 1, :]

    for s in range(train_data.shape[0]):
        # train_noise_samples[s] = train_noise_samples[s] * (config['dataset']['regain'] / np.maximum(util.rms(train_data[s, 1]), 0.00001))
        train_data[s, 1] = train_data[s, 1] * (config['dataset']['regain'] / np.maximum(util.rms(train_data[s, 1]), 0.00001))

    for s in range(test_data.shape[0]):

        # test_noise_samples[s] = test_noise_samples[s] * (config['dataset']['regain'] / np.maximum(util.rms(test_data[s, 1]), 0.00001))
        test_data[s, 1] = test_data[s, 1] * (config['dataset']['regain'] / np.maximum(util.rms(test_data[s, 1]), 0.00001))


    # train_data_output_1 = train_data[:, 1, :][:, model.get_padded_target_field_indices()]
    # train_data_output_2 = train_noise_samples[:, model.get_padded_target_field_indices()]

    test_data_output_1 = test_data[:, 1, :][:, model.get_padded_target_field_indices()]
    # test_data_output_2 = test_noise_samples[:, model.get_padded_target_field_indices()]

    # model.model.fit(train_data[:, 0, :], {'data_output_1': train_data_output_1}, batch_size=config['training']['batch_size'],
    #           epochs=config['training']['num_epochs'], validation_data=(test_data[:, 0, :], {'data_output_1': test_data_output_1}),
    #                 callbacks=model.get_callbacks())
    # model.model.fit(train_data[:, 0, :], {'data_output_1': train_data_output_1, 'data_output_2': train_data_output_2},
    #                 batch_size=config['training']['batch_size'],
    #                 epochs=config['training']['num_epochs'], validation_data=(
    #     test_data[:, 0, :], {'data_output_1': test_data_output_1, 'data_output_2': test_data_output_2}),
    #                 callbacks=model.get_callbacks())

    model.model.fit_generator(batch_generator(train_data, model.get_padded_target_field_indices()),
                              steps_per_epoch=int(config["training"]["num_train_samples"]/config["training"]["batch_size"]),
                              callbacks=model.get_callbacks(), validation_data=(test_data[:, 0, :], {'data_output_1': test_data_output_1}),
                              epochs=config['training']['num_epochs'])

if mode == "inference":

    load_checkpoint = None ## Path of the best model for test set
    audios_path = "/data_B/sanabria/premier_league/audios/" ## Path for the .wav files of the testing set
    output_folder_path = "" ## Path where you want the output to be stored

    model = models.DenoisingWavenet(config, load_checkpoint=load_checkpoint)
    batch_size = config['training']['batch_size']


    for filename in glob.glob(audios_path+"*"):
        input = util.load_wav(filename, config['dataset']['sample_rate'])
        output_filename_prefix = filename.split("/")[-1].split(".")[0] + "_"

        denoise.denoise_sample(model, input, batch_size, output_filename_prefix,
                               config['dataset']['sample_rate'], output_folder_path)