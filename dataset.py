"""#LIBRARIES IMPORT"""
import os
import glob
import numpy as np
import soundfile as sf
import scipy.signal
import matplotlib.pyplot as plt
import pickle
import math

"""#PATH TO DIRECTORIES"""

# Path to the root directory
root_dir = "/data/diop/noise_removal/"
# Path to only comentators audio files
only_com_audios = "/data_B/sanabria/noise_removal/only_commentators/"

"""#DICTIONNARIES CREATION"""

# creation of the dictionnary containing the time of the matches' second half beginning
videos_init = {}
for line in open(root_dir + "starts.txt", "r").readlines():
    video_id, init_first_half, init_second_half = line.split(" ")
    videos_init[video_id] = [float(init_first_half), float(init_second_half)]

MatchesForDataset = {}
for line in open(root_dir + "Matches for dataset.txt", "r").read().splitlines():
    audio_id, audio_set = line.split(" ")
    MatchesForDataset[audio_id] = audio_set

"""#USEFUL FUNCTIONS"""


def read_wav(filename):
    # Reads in a wav audio file, takes the first channel, converts the signal to float64 representation

    audio_signal, sample_rate = sf.read(filename)

    if audio_signal.ndim > 1:
        audio_signal = audio_signal[:, 0]

    if audio_signal.dtype != 'float64':
        audio_signal = wav_to_float(audio_signal)

    return audio_signal, sample_rate


def load_wav(wav_path, desired_sample_rate):
    sequence, sample_rate = read_wav(wav_path)
    sequence = ensure_sample_rate(sequence, desired_sample_rate, sample_rate)
    return sequence


def ensure_sample_rate(x, desired_sample_rate, file_sample_rate):
    if file_sample_rate != desired_sample_rate:
        return scipy.signal.resample_poly(x, desired_sample_rate, file_sample_rate)
    return x


def delete_half_time(audio_sequence, videos_init, id):
    init_second_half_in_frames = int(videos_init[id][1] * 16000)
    half_time = range(init_second_half_in_frames - (16 * 60 * 16000), init_second_half_in_frames + (10 * 16000))
    return np.delete(audio_sequence, half_time, axis=None)


# boolean function which returns true if it's train and false if it's test
def isTrain(file):
    id = id = file[-11:-4]
    return MatchesForDataset[id] == "Train"


"""#Audio Segmentation"""


def create_sample_array(file):
    id = file[:-4].split("/")[-1]
    original_audio_file = file.replace('only_commentators', 'original')
    print(original_audio_file)
    threshold = 0.01
    print("loading audios...")
    original_seq = load_wav(original_audio_file, 16000)
    only_com_seq = load_wav(file, 16000)
    print("deleting half times...")
    original_seq = delete_half_time(original_seq, videos_init, id)
    only_com_seq = delete_half_time(only_com_seq, videos_init, id)
    print("audios loaded...")

    # commentator talking sample
    k = 0
    sample_array = np.empty(shape=(1, 2, 7739))
    sample_array = np.delete(sample_array, (0), axis=0)
    silence_array = np.empty(shape=(1, 2, 7739))
    silence_array = np.delete(silence_array, (0), axis=0)

    while k < len(only_com_seq) - 7739:
        if abs(only_com_seq[k]) > threshold:
            sample_array = np.append(sample_array, [[original_seq[k:k + 7739], only_com_seq[k:k + 7739]]], axis=0)
            print k / 16000.
            k = k + 8000
        else:
            if np.max(only_com_seq[k:k + 7739]) < threshold:
                silence_array = np.append(silence_array, [[original_seq[k:k + 7739], only_com_seq[k:k + 7739]]], axis=0)
                k = k + 8000
            else:
                k = k + 25000

    print("sample array : ", np.shape(sample_array))
    print("silence array : ", np.shape(silence_array))

    sample_limit = int(np.floor(np.size(sample_array, 0) * 0.1 / 0.9))
    print("silence sample limit = ", sample_limit)
    if np.size(silence_array, 0) > 0 and sample_limit > 0:
        np.random.shuffle(silence_array)
        sample_array = np.append(sample_array, silence_array[0:sample_limit, :, :], axis=0)

    print("new sample array : ", np.shape(sample_array))
    return sample_array


"""# Dataset Creation"""


def create_dataset():
    print("initializing...")
    train_data = np.empty(shape=(1, 2, 7739))
    train_data = np.delete(train_data, (0), axis=0)
    test_data = np.empty(shape=(1, 2, 7739))
    test_data = np.delete(test_data, (0), axis=0)
    print("processing...")
    for s, file in enumerate(glob.glob(only_com_audios + "*")):
        print("... file ", s + 1, " : ", file, "...")
        if isTrain(file):
            train_data = np.append(train_data, create_sample_array(file), axis=0)
            print("...samples for train")
            print("---------------------------------------")
        else:
            test_data = np.append(test_data, create_sample_array(file), axis=0)
            print("...samples for test")
            print("---------------------------------------")

    sample_indices = np.random.randint(0, len(train_data), 2000)
    for idx in sample_indices:
        data_to_add_noise = train_data[idx, 0]
        RMS_s = math.sqrt(np.mean(data_to_add_noise ** 2))
        RMS_n = math.sqrt(RMS_s ** 2 / (pow(10, 15 / 20)))
        noise = np.random.normal(0, RMS_n, data_to_add_noise.shape[0])
        train_data[idx, 0] = train_data[idx, 0] + noise

    np.random.shuffle(train_data)
    print("files creation...")

    # pickle.dump(train_data, open(root_dir+"train_data.pkl", "wb"))
    # pickle.dump(test_data, open(root_dir+"test_data.pkl", "wb"))
    print("stored in : " + root_dir)
    print("download complete")


if __name__ == '__main__':
    create_dataset()
